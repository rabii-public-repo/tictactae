package com.tictactae;

import java.util.ArrayList;
import java.util.List;

public class Solution {

	static List<String> updateListByColumn(List<String> lines, int indexColumn) {
		List<String> resultList = new ArrayList<String>();
		for (String line : lines) {
			StringBuilder sb = new StringBuilder(line);
			sb.setCharAt(indexColumn, 'O');
			resultList.add(sb.toString());
		}
		return resultList;
	}

	static List<String> updateListByDiagonal(List<String> lines, int coordinate) {
		List<String> resultList = new ArrayList<String>();
		if (coordinate == 0) { // start from line 0
			for (String line : lines) {
				StringBuilder sb = new StringBuilder(line);
				sb.setCharAt(coordinate, 'O');
				coordinate++;
				resultList.add(sb.toString());
			}
		} else if (coordinate == 2) { // start from line 2
			for (String line : lines) {
				StringBuilder sb = new StringBuilder(line);
				sb.setCharAt(coordinate, 'O');
				coordinate--;
				resultList.add(sb.toString());
			}
		}
		return resultList;
	}

	static void print(List<String> lines) {
		List<String> finalList = getFinalList(lines);
		if (finalList != null) {
			for (String line : finalList) {
				System.out.println(line);
			}
		} else {
			System.out.println(false);
		}
	}

	static List<String> getFinalList(List<String> lines) {
		int indexLine = canPlayHorizontallyThenReturnIndexOfLine(lines);
		if (indexLine < 0) {// he cant paly horizontally
			int indexColumn = canPlayVerticallyThenReturnTheIndexOfColumn(lines);
			if (indexColumn < 0) { // he cant play vertically
				int coordinate = canPlayDiagonallyThenReturnCordinateOfStartPoint(lines);
				if (coordinate < 0) { // he cant play diagonnaly
					return null;
				} else {
					lines = updateListByDiagonal(lines, coordinate);
				}
			} else { // he can play vertically
				lines = updateListByColumn(lines, indexColumn);
			}
		} else {// he can play horizontally
			lines.set(indexLine, "OOO");
		}
		return lines;
	}

	static int canPlayHorizontallyThenReturnIndexOfLine(List<String> lines) {
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			int count = 0;
			for (int j = 0; j < 3; j++) {
				if (line.charAt(j) == 'X') {
					count--;
				}
				if (line.charAt(j) == 'O') {
					count++;
				}
			}
			if (count == 2) {
				return i;
			}
			if (count == -1 || count == -3) {
				return -10;
			}
		}
		return -1;
	}

	static int canPlayVerticallyThenReturnTheIndexOfColumn(List<String> lines) {
		Character[][] characters = convertListToArray(lines);

		for (int i = 0; i < 3; i++) {
			int count = 0;
			for (int j = 0; j < 3; j++) {
				if (characters[j][i] == 'X') {
					count--;
				} else if (characters[j][i] == 'O') {
					count++;
				}
			}
			if (count == 2) {
				return i;
			}
			if (count == -1 || count == -3) {
				return -10;
			}
		}
		return -1;
	}

	static int canPlayDiagonallyThenReturnCordinateOfStartPoint(List<String> lines) {
		Character[][] characters = convertListToArray(lines);
		int coordinate = -1;
		int i = 0, j = 0, count = 0;
		int k = 2, l = 0;
		while (i == j && i < 3) {
			if (characters[i][j] == 'X') {
				count--;
			}

			if (characters[i][j] == 'O') {
				count++;
			}
			i++;
			j++;
		}
		if (count == 2) {
			return 0;
		}
		count = 0;
		while (k >= 0) {
			if (characters[k][l] == 'X') {
				count--;
			}
			if (characters[k][l] == 'O') {
				count++;
			}
			k--;
			l++;
		}
		if (count == 2) {
			return 2;
		}
		if (count == -1 || count == -3) {
			return -10;
		}
		return coordinate;
	}

	static Character[][] convertListToArray(List<String> lines) {
		Character[][] characters = new Character[3][3];
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				characters[i][j] = line.charAt(j);
			}
		}
		return characters;
	}
}
